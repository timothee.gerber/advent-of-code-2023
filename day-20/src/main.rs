use std::{
    collections::{HashMap, VecDeque},
    str::FromStr,
};

use num::Integer;

#[derive(Debug)]
enum Module {
    FlipFlop(bool),
    Conjunction(HashMap<&'static str, bool>),
    Broadcast,
}

impl FromStr for Module {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(if s == "broadcaster" {
            Module::Broadcast
        } else if s.starts_with('%') {
            Module::FlipFlop(false)
        } else if s.starts_with('&') {
            Module::Conjunction(HashMap::new())
        } else {
            unreachable!()
        })
    }
}

fn part_1(input: &'static str) -> u64 {
    let mut modules = parse(input);
    let mut low = 0;
    let mut high = 0;

    for _ in 0..1000 {
        let mut queue = VecDeque::from([("button", "broadcaster", false)]);
        low += 1;

        while let Some((from, to, pulse)) = queue.pop_front() {
            match modules.get_mut(to) {
                Some((Module::Broadcast, next_modules)) => {
                    for next_module in next_modules {
                        queue.push_back((to, next_module, pulse));
                        if pulse {
                            high += 1;
                        } else {
                            low += 1;
                        }
                    }
                }
                Some((Module::FlipFlop(ref mut flip_flop), next_modules)) => {
                    if pulse {
                        continue;
                    }

                    *flip_flop = !*flip_flop;

                    for next_module in next_modules {
                        if *flip_flop {
                            high += 1;
                            queue.push_back((to, next_module, true));
                        } else {
                            queue.push_back((to, next_module, false));
                            low += 1;
                        }
                    }
                }
                Some((Module::Conjunction(ref mut inputs), next_modules)) => {
                    inputs.insert(from, pulse);
                    let enabled = inputs.values().all(|c| *c);
                    for next_module in next_modules {
                        if enabled {
                            low += 1;
                            queue.push_back((to, next_module, false));
                        } else {
                            queue.push_back((to, next_module, true));
                            high += 1;
                        }
                    }
                }
                None => {}
            }
        }
    }

    low * high
}

fn parse(input: &'static str) -> HashMap<&str, (Module, Vec<&str>)> {
    let mut modules = input
        .trim()
        .lines()
        .map(|line| {
            let (name, next_modules) = line.split_once(" -> ").unwrap();
            let module = name.parse().unwrap();

            (
                name.trim_start_matches('%').trim_start_matches('&'),
                (module, next_modules.split(", ").collect()),
            )
        })
        .collect::<HashMap<_, _>>();

    let mut from = HashMap::new();

    for (name, (_module, next_modules)) in &modules {
        for next_module in next_modules {
            if let Some((Module::Conjunction(_), _)) = modules.get(next_module) {
                let entry = from.entry(*next_module).or_insert(HashMap::new());
                (*entry).insert(*name, false);
            }
        }
    }

    for (name, entries) in from.drain() {
        modules.get_mut(name).unwrap().0 = Module::Conjunction(entries);
    }

    modules
}

fn part_2(input: &'static str) -> u64 {
    // rx is connected by bn only
    // bn is a conjunction, sending low only when all entires sends high
    // bn entries are pl, mz, lz, zm
    //
    // Algorithm: detect cycle for pl, mz, lz, zm and compute lcm of the values
    // to find the first time bn is sent high (ie, rx is sent low)

    let mut modules = parse(input);
    let mut cycles: HashMap<&str, u64> = HashMap::new();

    for idx in 1.. {
        if cycles.len() == 4 {
            return cycles.into_values().reduce(|acc, c| acc.lcm(&c)).unwrap();
        }

        let mut queue = VecDeque::from([("button", "broadcaster", false)]);

        while let Some((from, to, pulse)) = queue.pop_front() {
            if to == "rx" && !pulse {
                return idx;
            }

            match modules.get_mut(to) {
                Some((Module::Broadcast, next_modules)) => {
                    for next_module in next_modules {
                        queue.push_back((to, next_module, pulse));
                    }
                }
                Some((Module::FlipFlop(ref mut flip_flop), next_modules)) => {
                    if pulse {
                        continue;
                    }

                    *flip_flop = !*flip_flop;

                    for next_module in next_modules {
                        if *flip_flop {
                            queue.push_back((to, next_module, true));
                        } else {
                            queue.push_back((to, next_module, false));
                        }
                    }
                }
                Some((Module::Conjunction(ref mut inputs), next_modules)) => {
                    inputs.insert(from, pulse);
                    let enabled = inputs.values().all(|c| *c);
                    for next_module in next_modules {
                        if enabled {
                            queue.push_back((to, next_module, false));
                        } else {
                            queue.push_back((to, next_module, true));
                        }
                    }
                    if !enabled
                        && ["pl", "mz", "lz", "zm"].contains(&to)
                        && !cycles.contains_key(&to)
                    {
                        cycles.insert(to, idx);
                    }
                }
                None => {}
            }
        }
    }

    unreachable!()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example_1")), 32000000);
        assert_eq!(part_1(include_str!("example_2")), 11687500);
    }
}
