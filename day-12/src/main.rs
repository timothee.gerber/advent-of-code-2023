use std::collections::HashMap;

fn part_1(input: &str) -> usize {
    input
        .trim()
        .lines()
        .map(|line| parse_line(line, false))
        .map(|(status, sizes)| count_arrangements(&mut HashMap::new(), &status, &sizes, None))
        .sum()
}

fn part_2(input: &str) -> usize {
    input
        .trim()
        .lines()
        .map(|line| parse_line(line, true))
        .map(|(status, sizes)| count_arrangements(&mut HashMap::new(), &status, &sizes, None))
        .sum()
}

fn parse_line(line: &str, unfold: bool) -> (Vec<char>, Vec<usize>) {
    let (status, sizes) = line.split_once(' ').unwrap();

    let (status, sizes) = if unfold {
        let new_status = vec![status; 5].join("?");
        let new_sizes = vec![sizes; 5].join(",");
        (new_status, new_sizes)
    } else {
        (status.to_string(), sizes.to_string())
    };

    let status: Vec<char> = status.chars().collect();

    let sizes: Vec<usize> = sizes
        .split(',')
        .map(|n| n.parse::<usize>().unwrap())
        .collect();

    (status, sizes)
}

fn count_arrangements(
    cache: &mut HashMap<(usize, Option<usize>, usize), usize>,
    status: &[char],
    sizes: &[usize],
    current: Option<usize>,
) -> usize {
    if status.is_empty() {
        return match (current, sizes.len()) {
            (None, 0) => 1,
            (Some(x), 1) if x == sizes[0] => 1,
            _ => 0,
        };
    }

    if current.is_some() && sizes.is_empty() {
        return 0;
    }

    let cache_key = (status.len(), current, sizes.len());

    if let Some(n_arrangements) = cache.get(&cache_key) {
        return *n_arrangements;
    }

    let n_arrangements = match (status[0], current) {
        ('.', None) => count_arrangements(cache, &status[1..], sizes, None),
        ('.', Some(x)) => {
            if x != sizes[0] {
                0
            } else {
                count_arrangements(cache, &status[1..], &sizes[1..], None)
            }
        }
        ('?', None) => {
            count_arrangements(cache, &status[1..], sizes, None)
                + count_arrangements(cache, &status[1..], sizes, Some(1))
        }
        ('?', Some(x)) => {
            let mut n = count_arrangements(cache, &status[1..], sizes, Some(x + 1));
            if x == sizes[0] {
                n += count_arrangements(cache, &status[1..], &sizes[1..], None)
            }
            n
        }
        ('#', None) => count_arrangements(cache, &status[1..], sizes, Some(1)),
        ('#', Some(x)) => count_arrangements(cache, &status[1..], sizes, Some(x + 1)),
        _ => unreachable!(),
    };

    cache.insert(cache_key, n_arrangements);
    n_arrangements
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 21);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 525152);
    }
}
