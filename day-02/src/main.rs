use regex::Regex;

fn max_number_of_cubes(input: &str) -> (i64, i64, i64) {
    let r_re = Regex::new(r"(?<cubes>\d+) red").unwrap();
    let g_re = Regex::new(r"(?<cubes>\d+) green").unwrap();
    let b_re = Regex::new(r"(?<cubes>\d+) blue").unwrap();

    let r = r_re
        .captures_iter(input)
        .map(|c| c["cubes"].parse::<i64>().unwrap())
        .max()
        .unwrap_or(0);

    let g = g_re
        .captures_iter(input)
        .map(|c| c["cubes"].parse::<i64>().unwrap())
        .max()
        .unwrap_or(0);

    let b = b_re
        .captures_iter(input)
        .map(|c| c["cubes"].parse::<i64>().unwrap())
        .max()
        .unwrap_or(0);

    (r, g, b)
}

fn part_1(input: &str) -> usize {
    input
        .trim()
        .lines()
        .enumerate()
        .filter_map(|(i, line)| {
            let (r, g, b) = max_number_of_cubes(line);

            if r <= 12 && g <= 13 && b <= 14 {
                Some(i + 1)
            } else {
                None
            }
        })
        .sum()
}

fn part_2(input: &str) -> i64 {
    input
        .trim()
        .lines()
        .map(|line| {
            let (r, g, b) = max_number_of_cubes(line);

            r * g * b
        })
        .sum()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 8);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 2286);
    }
}
