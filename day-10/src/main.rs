use std::collections::HashSet;

fn parse(input: &str) -> Vec<Vec<char>> {
    input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect()
}

fn find_start(map: &[Vec<char>]) -> (i64, i64) {
    for y in 0..map.len() {
        for x in 0..map[0].len() {
            if map[y][x] == 'S' {
                return (y as i64, x as i64);
            }
        }
    }

    unreachable!()
}

fn directions(c: char) -> [(i64, i64); 2] {
    match c {
        '|' => [(-1, 0), (1, 0)],
        '-' => [(0, -1), (0, 1)],
        'L' => [(-1, 0), (0, 1)],
        'J' => [(-1, 0), (0, -1)],
        '7' => [(1, 0), (0, -1)],
        'F' => [(1, 0), (0, 1)],
        _ => unreachable!(),
    }
}

fn next(previous: (i64, i64), current: (i64, i64), c: char) -> (i64, i64) {
    for (dy, dx) in directions(c) {
        if current.0 + dy != previous.0 || current.1 + dx != previous.1 {
            return (current.0 + dy, current.1 + dx);
        }
    }

    unreachable!()
}

fn find_loop(map: &[Vec<char>]) -> (HashSet<(i64, i64)>, (i64, i64), (i64, i64)) {
    let (y_start, x_start) = find_start(map);

    let mut pipes = Vec::new();

    for (dy, dx) in [(0, 1), (1, 0), (-1, 0), (0, -1)] {
        let (y, x) = (y_start + dy, x_start + dx);
        if 0 <= y
            && y < map.len() as i64
            && 0 <= x
            && x < map[0].len() as i64
            && map[y as usize][x as usize] != '.'
        {
            for (dy, dx) in directions(map[y as usize][x as usize]) {
                if y + dy == y_start && x + dx == x_start {
                    pipes.push(Vec::from([(y_start, x_start), (y, x)]));
                }
            }
        }
    }

    loop {
        for i in 0..pipes.len() {
            let current = pipes[i][pipes[i].len() - 1];
            let (y, x) = next(
                pipes[i][pipes[i].len() - 2],
                current,
                map[current.0 as usize][current.1 as usize],
            );

            if 0 <= y
                && y < map.len() as i64
                && 0 <= x
                && x < map[0].len() as i64
                && map[y as usize][x as usize] != '.'
            {
                for pipe in pipes.iter() {
                    if *pipe.last().unwrap() == (y, x) {
                        return (
                            pipes[i]
                                .iter()
                                .chain(pipe.iter())
                                .copied()
                                .collect::<HashSet<_>>(),
                            pipe[1],
                            pipes[i][1],
                        );
                    }
                }

                pipes[i].push((y, x));
            }
        }
    }
}

fn part_1(input: &str) -> usize {
    let (loop_, _, _) = find_loop(&parse(input));

    loop_.len() / 2
}

fn replace_start(
    (y_start, x_start): (i64, i64),
    (y_1, x_1): (i64, i64),
    (y_2, x_2): (i64, i64),
) -> char {
    let mut diff = [
        (y_1 - y_start, x_1 - x_start),
        (y_2 - y_start, x_2 - x_start),
    ];

    diff.sort();

    match diff {
        [(-1, 0), (1, 0)] => '|',
        [(0, -1), (0, 1)] => '-',
        [(0, 1), (1, 0)] => 'F',
        [(-1, 0), (0, -1)] => 'J',
        [(0, -1), (1, 0)] => '7',
        [(-1, 0), (0, 1)] => 'L',
        _ => unreachable!(),
    }
}

fn part_2(input: &str) -> usize {
    let map = parse(input);
    let (y_start, x_start) = find_start(&map);

    let (loop_, first_direction, second_direction) = find_loop(&map);

    let mut new_map = vec![vec![' '; map[0].len()]; map.len()];
    for &(y, x) in &loop_ {
        new_map[y as usize][x as usize] = map[y as usize][x as usize];
    }

    new_map[y_start as usize][x_start as usize] =
        replace_start((y_start, x_start), first_direction, second_direction);

    new_map
        .iter()
        .map(|line| {
            let line = line
                .iter()
                .collect::<String>()
                .replace('-', "")
                .replace("F7", "")
                .replace("LJ", "")
                .replace("FJ", "|")
                .replace("L7", "|");

            line.split('|')
                .enumerate()
                .filter(|(i, _)| i % 2 == 1)
                .map(|(_, s)| s.len())
                .sum::<usize>()
        })
        .sum()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example_1")), 4);
        assert_eq!(part_1(include_str!("example_2")), 8);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example_1")), 1);
        assert_eq!(part_2(include_str!("example_2")), 1);
        assert_eq!(part_2(include_str!("example_3")), 4);
        assert_eq!(part_2(include_str!("example_4")), 8);
        assert_eq!(part_2(include_str!("example_5")), 10);
    }
}
