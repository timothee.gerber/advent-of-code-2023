use std::collections::HashMap;

fn parse_schematic(input: &str) -> (Vec<Vec<char>>, usize, usize) {
    let schematic = input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect::<Vec<Vec<char>>>();

    let height = schematic.len();
    let width = schematic[0].len();

    (schematic, height, width)
}

fn find_numbers(
    grid: &[Vec<char>],
    height: usize,
    width: usize,
) -> Vec<(usize, usize, usize, u32)> {
    let mut numbers = Vec::new();
    let mut acc = None;
    let mut start = None;

    for y in 0..height {
        for x in 0..width {
            if let Some(digit) = grid[y][x].to_digit(10) {
                if let Some(mut v) = acc {
                    v *= 10;
                    v += digit;
                    acc = Some(v);
                } else {
                    acc = Some(digit);
                    start = Some(x);
                }
            } else if let Some(v) = acc {
                numbers.push((y, start.unwrap(), x - start.unwrap(), v));
                acc = None;
                start = None;
            }
        }

        if let Some(v) = acc {
            numbers.push((y, start.unwrap(), width - start.unwrap(), v));
            acc = None;
            start = None;
        }
    }

    numbers
}

fn find_adjacent_symbols(
    grid: &[Vec<char>],
    height: usize,
    width: usize,
    y: usize,
    x: usize,
    l: usize,
) -> Vec<(char, usize, usize)> {
    let mut symbols = Vec::new();

    let y = y as i64;
    let x = x as i64;

    for y in y - 1..=y + 1 {
        for x in x - 1..=x + l as i64 {
            if 0 <= y
                && y < height as i64
                && 0 <= x
                && x < width as i64
                && grid[y as usize][x as usize] != '.'
                && !grid[y as usize][x as usize].is_ascii_digit()
            {
                symbols.push((grid[y as usize][x as usize], y as usize, x as usize));
            }
        }
    }

    symbols
}

fn part_1(input: &str) -> u32 {
    let (schematic, height, width) = parse_schematic(input);

    find_numbers(&schematic, height, width)
        .iter()
        .filter_map(|(y, x, l, v)| {
            if !find_adjacent_symbols(&schematic, height, width, *y, *x, *l).is_empty() {
                Some(v)
            } else {
                None
            }
        })
        .sum()
}

fn part_2(input: &str) -> u32 {
    let (schematic, height, width) = parse_schematic(input);
    let mut gears: HashMap<(usize, usize), Vec<u32>> = HashMap::new();

    for (y, x, l, v) in find_numbers(&schematic, height, width) {
        for (symbol, yy, xx) in find_adjacent_symbols(&schematic, height, width, y, x, l) {
            if symbol != '*' {
                continue;
            }

            let entry = gears.entry((yy, xx)).or_default();
            entry.push(v);
        }
    }

    gears
        .values()
        .filter_map(|v| {
            if v.len() == 2 {
                Some(v[0] * v[1])
            } else {
                None
            }
        })
        .sum()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 4361);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 467835);
    }
}
