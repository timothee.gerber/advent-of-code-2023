use std::{
    collections::{HashSet, VecDeque},
    ops::Add,
};

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Direction {
    fn delta(&self) -> (i64, i64) {
        match self {
            Direction::Up => (-1, 0),
            Direction::Right => (0, 1),
            Direction::Down => (1, 0),
            Direction::Left => (0, -1),
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
struct Pos {
    x: i64,
    y: i64,
}

impl Pos {
    fn is_valid(&self, grid: &[Vec<char>]) -> bool {
        0 <= self.x && self.x < grid[0].len() as i64 && 0 <= self.y && self.y < grid.len() as i64
    }

    fn next(&self, grid: &[Vec<char>], direction: Direction) -> Vec<(Direction, Pos)> {
        let obstacle = grid[self.y as usize][self.x as usize];

        let nexts = match obstacle {
            '.' => vec![(direction, *self + direction)],
            '/' => match direction {
                Direction::Up => vec![(Direction::Right, *self + (0, 1))],
                Direction::Right => vec![(Direction::Up, *self + (-1, 0))],
                Direction::Down => vec![(Direction::Left, *self + (0, -1))],
                Direction::Left => vec![(Direction::Down, *self + (1, 0))],
            },
            '\\' => match direction {
                Direction::Up => vec![(Direction::Left, *self + (0, -1))],
                Direction::Right => vec![(Direction::Down, *self + (1, 0))],
                Direction::Down => vec![(Direction::Right, *self + (0, 1))],
                Direction::Left => vec![(Direction::Up, *self + (-1, 0))],
            },
            '|' => match direction {
                Direction::Up | Direction::Down => vec![(direction, *self + direction)],
                Direction::Right | Direction::Left => vec![
                    (Direction::Down, *self + (1, 0)),
                    (Direction::Up, *self + (-1, 0)),
                ],
            },
            '-' => match direction {
                Direction::Up | Direction::Down => vec![
                    (Direction::Left, *self + (0, -1)),
                    (Direction::Right, *self + (0, 1)),
                ],
                Direction::Right | Direction::Left => vec![(direction, *self + direction)],
            },
            _ => unreachable!(),
        };

        nexts
            .into_iter()
            .filter(|(_dir, pos)| pos.is_valid(grid))
            .collect()
    }
}

impl Add<(i64, i64)> for Pos {
    type Output = Self;

    fn add(self, other: (i64, i64)) -> Self::Output {
        Self {
            x: self.x + other.1,
            y: self.y + other.0,
        }
    }
}

impl Add<Direction> for Pos {
    type Output = Self;

    fn add(self, other: Direction) -> Self::Output {
        self + other.delta()
    }
}

fn part_1(input: &str) -> usize {
    let grid = parse(input);

    follow_light(&grid, Direction::Right, Pos { x: 0, y: 0 })
}

fn part_2(input: &str) -> usize {
    let grid = parse(input);

    (0..grid.len())
        .map(|y| (Direction::Right, Pos { x: 0, y: y as i64 }))
        .chain((0..grid.len()).map(|y| {
            (
                Direction::Left,
                Pos {
                    x: grid[0].len() as i64 - 1,
                    y: y as i64,
                },
            )
        }))
        .chain((0..grid[0].len()).map(|x| (Direction::Down, Pos { x: x as i64, y: 0 })))
        .chain((0..grid[0].len()).map(|x| {
            (
                Direction::Up,
                Pos {
                    x: x as i64,
                    y: grid.len() as i64 - 1,
                },
            )
        }))
        .map(|(direction, pos)| follow_light(&grid, direction, pos))
        .max()
        .unwrap()
}

fn parse(input: &str) -> Vec<Vec<char>> {
    input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect()
}

fn follow_light(grid: &[Vec<char>], direction: Direction, position: Pos) -> usize {
    let mut to_visit = VecDeque::from([(direction, position)]);
    let mut visited = HashSet::new();
    let mut visited_without_direction = HashSet::new();

    while let Some((direction, position)) = to_visit.pop_front() {
        if visited.contains(&(direction, position)) {
            continue;
        }

        to_visit.extend(position.next(grid, direction));

        visited.insert((direction, position));
        visited_without_direction.insert(position);
    }

    visited_without_direction.len()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 46);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 51);
    }
}
