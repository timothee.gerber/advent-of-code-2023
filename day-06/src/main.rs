use regex::Regex;

fn parse(input: &str) -> (Vec<f64>, Vec<f64>) {
    let (times, distances) = input.trim().split_once('\n').unwrap();
    let re = Regex::new(r"\d+").unwrap();
    let times = re
        .captures_iter(times)
        .map(|c| c[0].parse::<f64>().unwrap())
        .collect::<Vec<f64>>();
    let distances = re
        .captures_iter(distances)
        .map(|c| c[0].parse::<f64>().unwrap())
        .collect::<Vec<f64>>();

    (times, distances)
}

fn part_1(input: &str) -> i64 {
    let (times, distances) = parse(input);

    let mut win_strategies = Vec::with_capacity(times.len());
    for (time, distance) in times.iter().zip(distances.iter()) {
        // s * (t - s) > d
        // -s^2 + s * t - d > 0
        // As there is a strict inegality, we use distance + 1 to find the
        // polynomial roots.
        let discriminant = time * time - 4. * (distance + 1.);

        if discriminant < 0. {
            win_strategies.push(0);
            continue;
        }

        let min = ((time - discriminant.sqrt()) / 2.0).ceil();
        let max = ((time + discriminant.sqrt()) / 2.0).floor();
        win_strategies.push((max - min) as i64 + 1);
    }

    win_strategies.iter().product()
}

fn part_2(input: &str) -> i64 {
    part_1(&input.replace(' ', ""))
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 288);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 71503);
    }
}
