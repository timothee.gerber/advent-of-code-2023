use std::collections::HashMap;

fn part_1(input: &str) -> usize {
    let mut grid = parse(input);

    tilt_north(&mut grid);

    grid.iter()
        .enumerate()
        .map(|(y, row)| (grid.len() - y) * row.iter().filter(|c| **c == 'O').count())
        .sum()
}

fn part_2(input: &str) -> usize {
    let mut grid = parse(input);

    let mut memory = HashMap::new();
    let mut n_cycles = 0;
    let mut previous_cycle = 0;

    memory.insert(grid.clone(), n_cycles);

    for n in 1..=1000000000 {
        tilt_north(&mut grid);
        tilt_west(&mut grid);
        tilt_south(&mut grid);
        tilt_east(&mut grid);

        if let Some(previous) = memory.get(&grid) {
            n_cycles = n;
            previous_cycle = *previous;
            break;
        }

        memory.insert(grid.clone(), n);
    }

    let missing_cycles = (1000000000 - n_cycles) % (n_cycles - previous_cycle);

    for _ in 0..missing_cycles {
        tilt_north(&mut grid);
        tilt_west(&mut grid);
        tilt_south(&mut grid);
        tilt_east(&mut grid);
    }

    grid.iter()
        .enumerate()
        .map(|(y, row)| (grid.len() - y) * row.iter().filter(|c| **c == 'O').count())
        .sum()
}

fn parse(input: &str) -> Vec<Vec<char>> {
    input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect()
}

fn tilt_north(grid: &mut [Vec<char>]) {
    for x in 0..grid[0].len() {
        let mut where_it_rolls = 0;
        for y in 0..grid.len() {
            match grid[y][x] {
                '.' => {}
                '#' => {
                    where_it_rolls = y + 1;
                }
                'O' => {
                    grid[y][x] = '.';
                    grid[where_it_rolls][x] = 'O';
                    where_it_rolls += 1;
                }
                _ => unreachable!(),
            }
        }
    }
}

fn tilt_west(grid: &mut [Vec<char>]) {
    for y in 0..grid.len() {
        let mut where_it_rolls = 0;
        for x in 0..grid[0].len() {
            match grid[y][x] {
                '.' => {}
                '#' => {
                    where_it_rolls = x + 1;
                }
                'O' => {
                    grid[y][x] = '.';
                    grid[y][where_it_rolls] = 'O';
                    where_it_rolls += 1;
                }
                _ => unreachable!(),
            }
        }
    }
}

fn tilt_south(grid: &mut [Vec<char>]) {
    for x in 0..grid[0].len() {
        let mut where_it_rolls = grid.len() - 1;
        for y in (0..grid.len()).rev() {
            match grid[y][x] {
                '.' => {}
                '#' => {
                    if y > 0 {
                        where_it_rolls = y - 1;
                    }
                }
                'O' => {
                    grid[y][x] = '.';
                    grid[where_it_rolls][x] = 'O';
                    if where_it_rolls > 0 {
                        where_it_rolls -= 1;
                    }
                }
                _ => unreachable!(),
            }
        }
    }
}

fn tilt_east(grid: &mut [Vec<char>]) {
    for y in 0..grid.len() {
        let mut where_it_rolls = grid[0].len() - 1;
        for x in (0..grid[0].len()).rev() {
            match grid[y][x] {
                '.' => {}
                '#' => {
                    if x > 0 {
                        where_it_rolls = x - 1;
                    }
                }
                'O' => {
                    grid[y][x] = '.';
                    grid[y][where_it_rolls] = 'O';
                    if where_it_rolls > 0 {
                        where_it_rolls -= 1;
                    }
                }
                _ => unreachable!(),
            }
        }
    }
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 136);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 64);
    }
}
