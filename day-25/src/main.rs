use std::collections::{HashMap, HashSet, VecDeque};

fn part_1(input: &str) -> usize {
    let mut graph: HashMap<_, HashSet<_>> = HashMap::new();
    let mut edges = HashSet::new();

    for line in input.trim().lines() {
        let (start, ends) = line.split_once(": ").unwrap();
        for end in ends.split_whitespace() {
            graph.entry(start).or_default().insert(end);
            graph.entry(end).or_default().insert(start);
            edges.insert(if start < end {
                (start, end)
            } else {
                (end, start)
            });
        }
    }

    let mut dot = String::from("graph {\n");
    for (start, end) in &edges {
        dot.push_str(&format!("    {} -- {};\n", start, end));
    }
    dot.push('}');

    std::fs::write("graph.dot", dot).unwrap();

    // Use graphviz command:
    // dot -Tsvg -Kneato graph.dot > graph.svg

    for (start, end) in [("pxp", "nqq"), ("dct", "kns"), ("jxb", "ksq")] {
        graph.get_mut(start).unwrap().remove(end);
        graph.get_mut(end).unwrap().remove(start);
    }

    let size = partition_size(&graph, "nxp");

    size * (graph.len() - size)
}

fn partition_size(graph: &HashMap<&str, HashSet<&str>>, start: &str) -> usize {
    let mut visited = HashSet::new();
    let mut to_visit = VecDeque::from([start]);

    while let Some(node) = to_visit.pop_front() {
        if visited.insert(node) {
            to_visit.extend(&graph[node]);
        }
    }

    visited.len()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 54);
    }
}
