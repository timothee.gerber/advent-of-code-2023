use linked_hash_map::LinkedHashMap;

fn part_1(input: &str) -> u64 {
    input
        .trim()
        .split(',')
        .map(|steps| hash(steps) as u64)
        .sum()
}

fn hash(steps: &str) -> u8 {
    let mut hash: u64 = 0;

    for c in steps.chars() {
        hash += c as u64;
        hash *= 17;
        hash %= 256;
    }

    hash as u8
}

fn part_2(input: &str) -> usize {
    let instructions = input.trim().split(',').collect::<Vec<_>>();
    let mut boxes: Vec<LinkedHashMap<&str, usize>> = vec![LinkedHashMap::new(); 256];

    for instruction in &instructions {
        if let Some((label, focal)) = instruction.split_once('=') {
            let focal = focal.parse::<usize>().unwrap();
            let box_number = hash(label);
            let lens = boxes[box_number as usize].entry(label).or_insert(focal);
            *lens = focal;
        } else {
            let label = instruction.trim_end_matches('-');
            let box_number = hash(label);
            boxes[box_number as usize].remove(label);
        }
    }

    let mut focusing_power = 0;

    for box_number in 0..256 {
        for (slot, lens) in boxes[box_number].values().enumerate() {
            focusing_power += (box_number + 1) * (slot + 1) * lens;
        }
    }

    focusing_power
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 1320);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 145);
    }
}
