use std::collections::HashSet;

fn parse_universe(input: &str) -> Vec<Vec<char>> {
    input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect()
}

fn find_expanding_spaces(universe: &[Vec<char>]) -> (HashSet<usize>, HashSet<usize>) {
    let mut rows = HashSet::new();
    let mut columns = HashSet::new();

    for (y, row) in universe.iter().enumerate() {
        if row.iter().all(|c| *c == '.') {
            rows.insert(y);
        }
    }

    'main_loop: for x in 0..universe[0].len() {
        for row in universe {
            if row[x] != '.' {
                continue 'main_loop;
            }
        }

        columns.insert(x);
    }

    (rows, columns)
}

fn find_galaxies(universe: &[Vec<char>]) -> Vec<(usize, usize)> {
    let mut galaxies = Vec::new();

    for (y, row) in universe.iter().enumerate() {
        for (x, element) in row.iter().enumerate() {
            if *element == '#' {
                galaxies.push((y, x));
            }
        }
    }

    galaxies
}

fn compute_distances(
    galaxies: Vec<(usize, usize)>,
    rows: HashSet<usize>,
    columns: HashSet<usize>,
    size_empty_space: usize,
) -> usize {
    let mut distances = 0;

    for (i, (y, x)) in galaxies.iter().enumerate() {
        for (yy, xx) in galaxies.iter().skip(i + 1) {
            let y_min = y.min(yy);
            let y_max = y.max(yy);
            let mut dy = y_max - y_min;

            for y in *y_min..*y_max {
                if rows.contains(&y) {
                    dy += size_empty_space - 1;
                }
            }

            let x_min = x.min(xx);
            let x_max = x.max(xx);
            let mut dx = x_max - x_min;

            for x in *x_min..*x_max {
                if columns.contains(&x) {
                    dx += size_empty_space - 1;
                }
            }

            distances += dy + dx;
        }
    }

    distances
}

fn solve(input: &str, size_empty_space: usize) -> usize {
    let universe = parse_universe(input);
    let (rows, columns) = find_expanding_spaces(&universe);
    let galaxies = find_galaxies(&universe);

    compute_distances(galaxies, rows, columns, size_empty_space)
}

fn main() {
    let input = include_str!("input");

    dbg!(solve(input, 2));
    dbg!(solve(input, 1000000));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(solve(include_str!("example"), 2), 374);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(solve(include_str!("example"), 10), 1030);
        assert_eq!(solve(include_str!("example"), 100), 8410);
    }
}
