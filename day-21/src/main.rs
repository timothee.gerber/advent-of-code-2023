use std::collections::HashSet;

fn part_1(input: &str, n_steps: usize) -> usize {
    let mut grid = input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect::<Vec<Vec<char>>>();

    let (y_start, x_start) = find_start(&mut grid);

    let mut destinations = [HashSet::from([(y_start, x_start)]), HashSet::new()];

    for step in 1..=n_steps {
        for (dy, dx) in [(-1, 0), (0, 1), (1, 0), (0, -1)] {
            let next_positions = destinations[(step - 1) % 2]
                .iter()
                .map(|(y, x)| (*y + dy, *x + dx))
                .filter(|(y, x)| {
                    0 <= *y
                        && *y < grid.len() as i64
                        && 0 <= *x
                        && *x < grid[0].len() as i64
                        && grid[*y as usize][*x as usize] == '.'
                        && !destinations[step % 2].contains(&(*y, *x))
                })
                .collect::<Vec<(i64, i64)>>();
            destinations[step % 2].extend(&next_positions);
        }
    }

    destinations[n_steps % 2].len()
}

fn find_start(grid: &mut [Vec<char>]) -> (i64, i64) {
    for y in 0..grid.len() {
        for x in 0..grid[0].len() {
            if grid[y][x] == 'S' {
                grid[y][x] = '.';
                return (y as i64, x as i64);
            }
        }
    }
    unreachable!()
}

fn part_2(input: &str, n_steps: usize) -> usize {
    let mut grid = input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect::<Vec<Vec<char>>>();

    let (y_start, x_start) = find_start(&mut grid);

    let mut destinations = [HashSet::from([(y_start, x_start)]), HashSet::new()];

    let mut sizes = Vec::from([1]);

    for step in 1..3 * 131 {
        for (dy, dx) in [(-1, 0), (0, 1), (1, 0), (0, -1)] {
            let next_positions = destinations[(step - 1) % 2]
                .iter()
                .map(|(y, x)| (*y + dy, *x + dx))
                .filter(|(y, x)| {
                    grid[(*y).rem_euclid(grid.len() as i64) as usize]
                        [(*x).rem_euclid(grid[0].len() as i64) as usize]
                        == '.'
                        && !destinations[step % 2].contains(&(*y, *x))
                })
                .collect::<Vec<(i64, i64)>>();
            destinations[step % 2].extend(&next_positions);
        }

        sizes.push(destinations[step % 2].len());
    }

    // Stolen formula...
    let (a, b, c) = (sizes[65], sizes[65 + 131], sizes[65 + 2 * 131]);
    let n = n_steps / 131;

    a + (b - a) * n + (a + c - 2 * b) * (n * (n - 1) / 2)
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input, 64));
    dbg!(part_2(input, 26501365));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example"), 0), 1);
        assert_eq!(part_1(include_str!("example"), 1), 2);
        assert_eq!(part_1(include_str!("example"), 2), 4);
        assert_eq!(part_1(include_str!("example"), 3), 6);
        assert_eq!(part_1(include_str!("example"), 6), 16);
    }
}
