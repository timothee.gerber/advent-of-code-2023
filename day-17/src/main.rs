use std::collections::{BinaryHeap, HashMap};

fn part_1(input: &str) -> i64 {
    let grid = parse(input);

    dijkstra(grid, 1, 3)
}

fn part_2(input: &str) -> i64 {
    let grid = parse(input);

    dijkstra(grid, 4, 10)
}

fn parse(input: &str) -> Vec<Vec<i64>> {
    input
        .trim()
        .lines()
        .map(|line| {
            line.chars()
                .map(|c| c.to_digit(10).unwrap() as i64)
                .collect()
        })
        .collect()
}

fn dijkstra(grid: Vec<Vec<i64>>, min_distance: i64, max_distance: i64) -> i64 {
    let mut costs = HashMap::new();
    let mut queue = BinaryHeap::from([(0, (0, 0), (0, 0))]);

    let height = grid.len();
    let width = grid[0].len();

    while let Some((cost, (y, x), (dy, dx))) = queue.pop() {
        if y == height - 1 && x == width - 1 {
            return -cost;
        }

        if costs.get(&((y, x), (dy, dx))).is_some_and(|&c| c > cost) {
            continue;
        }

        for (ndy, ndx) in [(-1, 0), (0, 1), (1, 0), (0, -1)] {
            if (ndy, ndx) == (dy, dx) || (ndy, ndx) == (-dy, -dx) {
                continue;
            }

            let mut next_cost = cost;

            for distance in 1..=max_distance {
                let next_y = (y as i64 + ndy * distance) as usize;
                let next_x = (x as i64 + ndx * distance) as usize;

                if next_y >= height || next_x >= width {
                    continue;
                }

                next_cost -= grid[next_y][next_x];

                if distance < min_distance {
                    continue;
                }

                if next_cost
                    > *costs
                        .get(&((next_y, next_x), (ndy, ndx)))
                        .unwrap_or(&i64::MIN)
                {
                    costs.insert(((next_y, next_x), (ndy, ndx)), next_cost);
                    queue.push((next_cost, (next_y, next_x), (ndy, ndx)));
                }
            }
        }
    }

    unreachable!()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example_1")), 102);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example_1")), 94);
        assert_eq!(part_2(include_str!("example_2")), 71);
    }
}
