fn part_1(input: &str) -> usize {
    input
        .trim()
        .split("\n\n")
        .map(|pattern| find_reflections(pattern, 0))
        .sum()
}

fn part_2(input: &str) -> usize {
    input
        .trim()
        .split("\n\n")
        .map(|pattern| find_reflections(pattern, 1))
        .sum()
}

fn origin_of_symmetry(lines: &[Vec<char>], desired_errors: usize) -> Option<usize> {
    for start in 0..lines.len() - 1 {
        let mut errors = 0;

        for y in 0..=start {
            if start + y + 1 >= lines.len() {
                break;
            }
            for x in 0..lines[0].len() {
                if lines[start - y][x] != lines[start + y + 1][x] {
                    errors += 1;
                }
            }
        }

        if errors == desired_errors {
            return Some(start + 1);
        }
    }

    None
}

fn find_reflections(pattern: &str, desired_errors: usize) -> usize {
    let rows = pattern
        .lines()
        .map(|line| line.chars().collect())
        .collect::<Vec<Vec<char>>>();

    if let Some(points) = origin_of_symmetry(&rows, desired_errors) {
        return 100 * points;
    }

    let columns = (0..rows[0].len())
        .map(|x| (0..rows.len()).map(|y| rows[y][x]).collect::<Vec<char>>())
        .collect::<Vec<Vec<char>>>();

    if let Some(points) = origin_of_symmetry(&columns, desired_errors) {
        return points;
    }

    unreachable!();
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 405);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 400);
    }
}
