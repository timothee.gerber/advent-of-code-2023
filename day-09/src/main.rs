fn parse(input: &str) -> Vec<Vec<i64>> {
    input
        .trim()
        .lines()
        .map(|line| {
            line.split(' ')
                .map(|n| n.parse::<i64>().unwrap())
                .collect::<Vec<i64>>()
        })
        .collect()
}

fn predict(numbers: &[i64]) -> (i64, i64) {
    let derive = numbers
        .windows(2)
        .map(|v| v[1] - v[0])
        .collect::<Vec<i64>>();

    if derive.iter().all(|x| *x == 0) {
        (numbers[0], *numbers.last().unwrap())
    } else {
        (
            numbers[0] - predict(&derive).0,
            *numbers.last().unwrap() + predict(&derive).1,
        )
    }
}

fn part_1(input: &str) -> i64 {
    parse(input)
        .iter()
        .map(|integers| predict(&integers).1)
        .sum()
}

fn part_2(input: &str) -> i64 {
    parse(input)
        .iter()
        .map(|integers| predict(&integers).0)
        .sum()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 114);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 2);
    }
}
