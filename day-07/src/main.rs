fn strength(s: &str, joker: bool) -> [i64; 6] {
    let mut strength = [0; 6];

    if joker {
        strength[0] = combinaison_strength_with_joker(s);
    } else {
        strength[0] = combinaison_strength(s);
    }

    for (idx, c) in s.char_indices() {
        strength[idx + 1] = card_strength(c, joker);
    }

    strength
}

fn combinaison_strength(s: &str) -> i64 {
    let mut counts = [0; 15];

    for c in s.chars() {
        counts[card_strength(c, false) as usize] += 1;
    }

    counts.sort_unstable_by(|a, b| b.cmp(a));

    match counts[0] {
        5 => 600,
        4 => 500,
        3 => match counts[1] {
            2 => 400,
            _ => 300,
        },
        2 => match counts[1] {
            2 => 200,
            _ => 100,
        },
        _ => 0,
    }
}

fn combinaison_strength_with_joker(s: &str) -> i64 {
    let mut counts = [0; 15];

    for c in s.chars() {
        counts[card_strength(c, true) as usize] += 1;
    }

    let jokers = counts[1];
    counts[1] = 0;

    counts.sort_unstable_by(|a, b| b.cmp(a));

    match counts[0] + jokers {
        5 => 600,
        4 => 500,
        3 => match counts[1] {
            2 => 400,
            _ => 300,
        },
        2 => match counts[1] {
            2 => 200,
            _ => 100,
        },
        _ => 0,
    }
}

fn card_strength(c: char, joker: bool) -> i64 {
    match c {
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        'T' => 10,
        'J' => match joker {
            true => 1,
            false => 11,
        },
        'Q' => 12,
        'K' => 13,
        'A' => 14,
        _ => unreachable!(),
    }
}

fn part_1(input: &str) -> i64 {
    let mut cards = input
        .trim()
        .lines()
        .map(|l| {
            let (cards, bid) = l.split_once(' ').unwrap();

            (strength(cards, false), bid.parse::<i64>().unwrap())
        })
        .collect::<Vec<_>>();

    cards.sort();

    cards
        .iter()
        .enumerate()
        .map(|(idx, (_, bid))| (idx as i64 + 1) * bid)
        .sum()
}

fn part_2(input: &str) -> i64 {
    let mut cards = input
        .trim()
        .lines()
        .map(|l| {
            let (cards, bid) = l.split_once(' ').unwrap();

            (strength(cards, true), bid.parse::<i64>().unwrap())
        })
        .collect::<Vec<_>>();

    cards.sort();

    cards
        .iter()
        .enumerate()
        .map(|(idx, (_, bid))| (idx as i64 + 1) * bid)
        .sum()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 6440);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 5905);
    }
}
