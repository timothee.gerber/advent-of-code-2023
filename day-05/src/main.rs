use std::str::FromStr;

struct Map(Vec<(i64, i64, i64)>);

impl Map {
    fn convert(&self, input: i64) -> i64 {
        for (start, end, start_destination) in &self.0 {
            if *start <= input && input < *end {
                return start_destination + input - start;
            }
        }

        input
    }
}

struct Maps(Vec<Map>);

impl FromStr for Maps {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Maps(
            s.trim()
                .split("\n\n")
                .map(|g| {
                    let mut map = g
                        .lines()
                        .skip(1)
                        .map(|l| {
                            let values = l
                                .trim()
                                .split(' ')
                                .map(|n| n.parse::<i64>().unwrap())
                                .collect::<Vec<i64>>();
                            (values[1], values[1] + values[2], values[0])
                        })
                        .collect::<Vec<(i64, i64, i64)>>();
                    map.sort();
                    Map(map)
                })
                .collect::<Vec<Map>>(),
        ))
    }
}

impl Maps {
    fn convert(&self, seed: i64) -> (i64, i64, i64, i64, i64, i64, i64, i64) {
        let mut current_idx = seed;
        let mut indexes = [seed; 8];

        for (i, map) in self.0.iter().enumerate() {
            current_idx = map.convert(current_idx);
            indexes[i + 1] = current_idx;
        }

        indexes.into()
    }
}

fn part_1(input: &str) -> i64 {
    let (seeds, maps) = input.split_once("\n\n").unwrap();
    let seeds = seeds
        .split_once(": ")
        .unwrap()
        .1
        .split(' ')
        .map(|n| n.parse().unwrap())
        .collect::<Vec<i64>>();
    let maps = maps.parse::<Maps>().unwrap();

    let seeds = seeds.iter().map(|s| maps.convert(*s)).collect::<Vec<_>>();

    seeds.iter().map(|s| s.7).min().unwrap()
}

fn part_2(input: &str) -> i64 {
    let (seeds_pairs, maps) = input.split_once("\n\n").unwrap();
    let seeds_pairs = seeds_pairs
        .split_once(": ")
        .unwrap()
        .1
        .split(' ')
        .map(|n| n.parse().unwrap())
        .collect::<Vec<i64>>();
    let maps = maps.parse::<Maps>().unwrap();

    let mut min = i64::MAX;
    for i in 0..seeds_pairs.len() / 2 {
        let start = seeds_pairs[2 * i];
        let n = seeds_pairs[2 * i + 1];
        for seed in start..start + n {
            let location = maps.convert(seed).7;
            if location < min {
                min = location;
            }
        }
    }
    min
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 35);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 46);
    }
}
