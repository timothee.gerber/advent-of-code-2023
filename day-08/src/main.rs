use num::Integer;
use std::collections::HashMap;

fn parse(input: &str) -> (&str, HashMap<&str, (&str, &str)>) {
    let (instructions, map) = input.trim().split_once("\n\n").unwrap();

    let map = map
        .lines()
        .map(|l| {
            let (name, directions) = l.split_once(" = (").unwrap();
            let (left, right) = directions.trim_end_matches(")").split_once(", ").unwrap();

            (name, (left, right))
        })
        .collect::<HashMap<&str, (&str, &str)>>();

    (instructions, map)
}

fn part_1(input: &str) -> i64 {
    let (instructions, map) = parse(input);
    let mut steps = 0;
    let mut position = "AAA";

    for direction in instructions.chars().cycle() {
        if position == "ZZZ" {
            break;
        }

        steps += 1;
        let (left, right) = map
            .get(position)
            .expect("all positions should be on the map");

        match direction {
            'L' => position = left,
            'R' => position = right,
            _ => unreachable!(),
        }
    }

    steps
}

fn part_2(input: &str) -> i64 {
    let (instructions, map) = parse(input);
    let mut steps = 0;
    let mut positions = map
        .keys()
        .filter(|name| name.ends_with('A'))
        .map(|name| *name)
        .collect::<Vec<&str>>();

    // Previous code experiment shows that distance between A -> Z is the same
    // that further cycles Z -> Z. So the problem consists in finding all
    // cycles and them computing the Least Commun Multiplier between all those
    // numbers.
    let mut cycles = vec![None; positions.len()];

    for direction in instructions.chars().cycle() {
        if cycles.iter().all(|c| c.is_some()) {
            break;
        }

        steps += 1;
        for i in 0..positions.len() {
            let (left, right) = map
                .get(positions[i])
                .expect("all positions should be on the map");

            let next_position = match direction {
                'L' => left,
                'R' => right,
                _ => unreachable!(),
            };

            if next_position.ends_with('Z') && cycles[i].is_none() {
                cycles[i] = Some(steps);
            }

            positions[i] = next_position;
        }
    }

    cycles
        .iter()
        .map(|c| c.unwrap() as i64)
        .reduce(|acc, c| acc.lcm(&c))
        .unwrap()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example_1")), 2);
        assert_eq!(part_1(include_str!("example_2")), 6);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example_3")), 6);
    }
}
