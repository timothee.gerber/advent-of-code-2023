#!/bin/sh

source ./.env

cargo new "day-$1"
cat > "day-$1/src/main.rs" <<EOF
fn part_1(input: &str) -> i64 {
    0
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    // dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 0);
    }

    // #[test]
    // fn test_part_2() {
    //     assert_eq!(part_2(include_str!("example")), 0);
    // }
}
EOF
touch "day-$1/src/example"
n_day=$(echo "$1" | sed 's/0*\([0-9]*\)/\1/')
curl -o "day-$1/src/input" --cookie "session=$SESSION" https://adventofcode.com/2023/day/${n_day}/input
