use std::collections::{HashMap, HashSet};

fn count_points(line: &str) -> usize {
    let (_, numbers) = line.trim().split_once(": ").unwrap();

    let (winning_numbers, numbers_i_have) = numbers.split_once(" | ").unwrap();

    let winning_numbers = winning_numbers
        .split(' ')
        .filter_map(|n| if !n.is_empty() { n.parse().ok() } else { None })
        .collect::<HashSet<i64>>();

    let numbers_i_have = numbers_i_have
        .split(' ')
        .filter_map(|n| if !n.is_empty() { n.parse().ok() } else { None })
        .collect::<HashSet<i64>>();

    winning_numbers.intersection(&numbers_i_have).count()
}

fn part_1(input: &str) -> i64 {
    input
        .trim()
        .lines()
        .map(|line| {
            let points = count_points(line);

            if points > 0 {
                2_i64.pow(points as u32 - 1)
            } else {
                0
            }
        })
        .sum()
}

fn part_2(input: &str) -> i64 {
    let mut copies = HashMap::new();
    for i in 0..input.trim().lines().count() {
        copies.insert(i, 1);
    }

    for (i, line) in input.trim().lines().enumerate() {
        let n_current_copies = *copies.get(&i).unwrap();

        let points = count_points(line);
        for next_card in i + 1..=i + points {
            copies
                .entry(next_card)
                .and_modify(|n| *n += n_current_copies);
        }
    }
    copies.values().sum()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 13);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 30);
    }
}
