fn part_1(input: &str) -> i64 {
    input
        .trim()
        .lines()
        .map(|line| {
            let digits = line
                .chars()
                .filter(|c| c.is_digit(10))
                .collect::<Vec<char>>();
            let number = format!("{}{}", digits.first().unwrap(), digits.last().unwrap());
            number.parse::<i64>().unwrap()
        })
        .sum()
}

fn part_2(input: &str) -> i64 {
    input
        .trim()
        .lines()
        .map(|line| {
            let digits = [
                ("1", "1"),
                ("2", "2"),
                ("3", "3"),
                ("4", "4"),
                ("5", "5"),
                ("6", "6"),
                ("7", "7"),
                ("8", "8"),
                ("9", "9"),
                ("one", "1"),
                ("two", "2"),
                ("three", "3"),
                ("four", "4"),
                ("five", "5"),
                ("six", "6"),
                ("seven", "7"),
                ("eight", "8"),
                ("nine", "9"),
            ];

            let mut first_digit = None;
            let mut first_index = None;

            let mut last_digit = None;
            let mut last_index = None;

            for (letters, digit) in &digits {
                if let Some(index) = line.find(letters) {
                    if first_index.is_none() || first_index.unwrap() > index {
                        first_digit = Some(digit);
                        first_index = Some(index);
                    }
                }

                if let Some(index) = line.rfind(letters) {
                    if last_index.is_none() || last_index.unwrap() < index {
                        last_digit = Some(digit);
                        last_index = Some(index);
                    }
                }
            }
            let number = format!("{}{}", first_digit.unwrap(), last_digit.unwrap());
            number.parse::<i64>().unwrap()
        })
        .sum()
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example_1")), 142);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example_2")), 281);
    }
}
