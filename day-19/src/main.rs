use std::collections::{HashMap, VecDeque};

fn part_1(input: &str) -> i64 {
    let (workflows, parts) = parse(input);

    let mut sum = 0;

    'part: for part in &parts {
        let mut workflow_name = String::from("in");
        'workflow: loop {
            let rules = workflows.get(&workflow_name).unwrap();

            for (destination, condition) in rules {
                if is_met(condition, part) {
                    if destination == "A" {
                        sum += part.values().sum::<i64>();
                        continue 'part;
                    } else if destination == "R" {
                        continue 'part;
                    } else {
                        workflow_name = destination.to_string();
                        continue 'workflow;
                    }
                }
            }
        }
    }

    sum
}

fn parse(
    input: &str,
) -> (
    HashMap<String, Vec<(String, Option<(String, char, i64)>)>>,
    Vec<HashMap<String, i64>>,
) {
    let (workflows, parts) = input.trim().split_once("\n\n").unwrap();

    let workflows = workflows
        .lines()
        .map(|line| {
            let (name, rules) = line.split_once('{').unwrap();

            let rules = rules[..rules.len() - 1]
                .split(',')
                .map(|rule| {
                    if !rule.contains(':') {
                        (rule.to_string(), None)
                    } else {
                        let (rule, name) = rule.split_once(':').unwrap();

                        (
                            name.to_string(),
                            Some((
                                rule.chars().nth(0).unwrap().to_string(),
                                rule.chars().nth(1).unwrap(),
                                rule[2..].parse().unwrap(),
                            )),
                        )
                    }
                })
                .collect();

            (name.to_string(), rules)
        })
        .collect();

    let parts = parts
        .lines()
        .map(|line| {
            line[1..line.len() - 1]
                .split(',')
                .map(|category| {
                    let (key, value) = category.split_once("=").unwrap();
                    (key.to_string(), value.parse::<i64>().unwrap())
                })
                .collect()
        })
        .collect();

    (workflows, parts)
}

fn is_met(condition: &Option<(String, char, i64)>, part: &HashMap<String, i64>) -> bool {
    if condition.is_none() {
        return true;
    }

    let (label, test, value) = condition.clone().unwrap();

    match test {
        '>' => *part.get(&label).unwrap() > value,
        '<' => *part.get(&label).unwrap() < value,
        _ => unreachable!(),
    }
}

fn part_2(input: &str) -> usize {
    let (workflows, _) = parse(input);
    let mut accepted = 0;
    let mut ranges = VecDeque::from([(
        "in".to_string(),
        HashMap::from([
            ("x".to_string(), 1..=4000i64),
            ("m".to_string(), 1..=4000i64),
            ("a".to_string(), 1..=4000i64),
            ("s".to_string(), 1..=4000i64),
        ]),
    )]);

    while let Some((workflow, range)) = ranges.pop_front() {
        if workflow == "A" {
            accepted += range.into_values().map(|r| r.count()).product::<usize>();
            continue;
        } else if workflow == "R" {
            continue;
        }

        let mut range = range;

        for (next, condition) in workflows.get(&workflow).unwrap() {
            let (met, not_met) = separate(condition, range);
            if let Some(new_range) = met {
                ranges.push_back((next.to_string(), new_range));
            }

            if not_met.is_none() {
                break;
            } else {
                range = not_met.unwrap();
            }
        }
    }

    accepted
}

fn separate(
    condition: &Option<(String, char, i64)>,
    ranges: HashMap<String, std::ops::RangeInclusive<i64>>,
) -> (
    Option<HashMap<String, std::ops::RangeInclusive<i64>>>,
    Option<HashMap<String, std::ops::RangeInclusive<i64>>>,
) {
    if let Some((label, operation, value)) = condition {
        let range = ranges.get(label).unwrap();

        match operation {
            '<' => {
                if range.end() < value {
                    (Some(ranges), None)
                } else if range.start() > value {
                    (None, Some(ranges))
                } else {
                    let mut met = ranges.clone();
                    let mut not_met = ranges.clone();
                    met.insert(label.clone(), *range.start()..=(value - 1));
                    not_met.insert(label.clone(), *value..=*range.end());
                    (Some(met), Some(not_met))
                }
            }
            '>' => {
                if range.start() > value {
                    (Some(ranges), None)
                } else if range.end() < value {
                    (None, Some(ranges))
                } else {
                    let mut met = ranges.clone();
                    let mut not_met = ranges.clone();
                    met.insert(label.clone(), (value + 1)..=*range.end());
                    not_met.insert(label.clone(), *range.start()..=*value);
                    (Some(met), Some(not_met))
                }
            }
            _ => unreachable!(),
        }
    } else {
        (Some(ranges), None)
    }
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 19114);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 167409079868000);
    }
}
