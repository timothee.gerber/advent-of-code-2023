use std::ops::RangeInclusive;
use z3::ast::{Ast, Int, Real};
use z3::SatResult;

fn part_1(input: &str, test_area: RangeInclusive<f64>) -> i64 {
    let hailstones = parse(input);
    let mut intersections = 0;

    for i in 0..hailstones.len() {
        for j in (i + 1)..hailstones.len() {
            let intersection = find_future_intersection(hailstones[i], hailstones[j]);
            if let Some((x, y)) = intersection {
                if test_area.contains(&x) && test_area.contains(&y) {
                    intersections += 1;
                }
            }
        }
    }

    intersections
}

fn parse(input: &str) -> Vec<([i64; 3], [i64; 3])> {
    input
        .trim()
        .lines()
        .map(|line| {
            let (pos, speed) = line.split_once(" @ ").unwrap();
            let pos = pos
                .split(", ")
                .map(|n| n.trim().parse::<_>().unwrap())
                .collect::<Vec<_>>();
            let speed = speed
                .split(", ")
                .map(|n| n.trim().parse::<_>().unwrap())
                .collect::<Vec<_>>();
            ([pos[0], pos[1], pos[2]], [speed[0], speed[1], speed[2]])
        })
        .collect()
}

fn find_future_intersection(
    a: ([i64; 3], [i64; 3]),
    b: ([i64; 3], [i64; 3]),
) -> Option<(f64, f64)> {
    let (x1, y1) = (a.0[0] as f64, a.0[1] as f64);
    let (vx1, vy1) = (a.1[0] as f64, a.1[1] as f64);
    let (x2, y2) = (x1 + 1000. * vx1, y1 + 1000. * vy1);
    let (x3, y3) = (b.0[0] as f64, b.0[1] as f64);
    let (vx3, vy3) = (b.1[0] as f64, b.1[1] as f64);
    let (x4, y4) = (x3 + 1000. * vx3, y3 + 1000. * vy3);

    let d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

    if d != 0. {
        let x = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        let y = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);
        let (x, y) = (x / d, y / d);

        //  { x = t * vx1 + x1;
        //  { y = t * vy1 + y1;
        // t = (y - y1) / vy1;

        if (y - y1) / vy1 >= 0. && (y - y3) / vy3 >= 0. {
            return Some((x, y));
        }
    }

    None
}

fn part_2(input: &str) -> i64 {
    let hailstones = parse(input);

    // system with 9 equations and 9 unknowns

    // x + t1 * vx = x1 + t1 * vx1
    // y + t1 * vy = y1 + t1 * vy1
    // z + t1 * vz = z1 + t1 * vz1

    // x + t2 * vx = x2 + t2 * vx2
    // y + t2 * vy = y2 + t2 * vy2
    // z + t2 * vz = z2 + t2 * vz2

    // x + t3 * vx = x3 + t3 * vx3
    // y + t3 * vy = y3 + t3 * vy3
    // z + t3 * vz = z3 + t3 * vz3

    let z3_conf = z3::Config::new();
    let ctx = z3::Context::new(&z3_conf);

    let x = Real::new_const(&ctx, "x");
    let y = Real::new_const(&ctx, "y");
    let z = Real::new_const(&ctx, "z");
    let vx = Real::new_const(&ctx, "vx");
    let vy = Real::new_const(&ctx, "vy");
    let vz = Real::new_const(&ctx, "vz");

    let solver = z3::Solver::new(&ctx);

    for (i, hailstone) in hailstones.iter().enumerate().take(3) {
        let t = Real::new_const(&ctx, format!("t{i}"));
        let xx = Real::from_int(&Int::from_i64(&ctx, hailstone.0[0] as i64));
        let yy = Real::from_int(&Int::from_i64(&ctx, hailstone.0[1] as i64));
        let zz = Real::from_int(&Int::from_i64(&ctx, hailstone.0[2] as i64));
        let vxx = Real::from_int(&Int::from_i64(&ctx, hailstone.1[0] as i64));
        let vyy = Real::from_int(&Int::from_i64(&ctx, hailstone.1[1] as i64));
        let vzz = Real::from_int(&Int::from_i64(&ctx, hailstone.1[2] as i64));

        solver.assert(&t.gt(&Real::from_int(&Int::from_i64(&ctx, 0))));
        solver.assert(
            &Real::add(&ctx, &[&x, &Real::mul(&ctx, &[&t, &vx])])
                ._eq(&Real::add(&ctx, &[&xx, &Real::mul(&ctx, &[&t, &vxx])])),
        );
        solver.assert(
            &Real::add(&ctx, &[&y, &Real::mul(&ctx, &[&t, &vy])])
                ._eq(&Real::add(&ctx, &[&yy, &Real::mul(&ctx, &[&t, &vyy])])),
        );
        solver.assert(
            &Real::add(&ctx, &[&z, &Real::mul(&ctx, &[&t, &vz])])
                ._eq(&Real::add(&ctx, &[&zz, &Real::mul(&ctx, &[&t, &vzz])])),
        );
    }
    assert_eq!(solver.check(), SatResult::Sat);

    let model = solver.get_model().unwrap();
    let xxx = model
        .eval(&x, true)
        .unwrap()
        .as_real()
        .and_then(|(n, d)| Some(n as f64 / d as f64))
        .unwrap();
    let yyy = model
        .eval(&y, true)
        .unwrap()
        .as_real()
        .and_then(|(n, d)| Some(n as f64 / d as f64))
        .unwrap();
    let zzz = model
        .eval(&z, true)
        .unwrap()
        .as_real()
        .and_then(|(n, d)| Some(n as f64 / d as f64))
        .unwrap();

    (xxx + yyy + zzz) as i64
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input, 200000000000000f64..=400000000000000f64)); // 20434
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example"), 7f64..=27f64), 2);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 47);
    }
}
