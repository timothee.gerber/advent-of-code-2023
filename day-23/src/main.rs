use std::collections::{HashMap, HashSet, VecDeque};

fn part_1(input: &str) -> i64 {
    let (graph, end) = parse(input, true);
    find_longest_path(&graph, end)
}

fn part_2(input: &str) -> i64 {
    let (graph, end) = parse(input, false);
    find_longest_path(&graph, end)
}

fn parse(input: &str, slippery: bool) -> (HashMap<(i64, i64), Vec<((i64, i64), i64)>>, (i64, i64)) {
    let mut grid = input
        .trim()
        .lines()
        .map(|line| line.chars().collect())
        .collect::<Vec<Vec<char>>>();
    grid[0][1] = '#';
    let (y_end, x_end) = (grid.len() - 1, grid[0].len() - 2);
    grid[y_end][x_end] = '#';

    let mut graph = HashMap::new();

    for y in 0..grid.len() {
        for x in 0..grid[0].len() {
            if grid[y][x] == '#' {
                continue;
            }

            let neighbors = directions(grid[y][x], slippery)
                .iter()
                .map(|(dy, dx)| (((y as i64 + dy), (x as i64 + dx)), 1))
                .filter(|((y, x), _)| grid[*y as usize][*x as usize] != '#')
                .collect::<Vec<((i64, i64), i64)>>();

            graph.insert((y as i64, x as i64), neighbors);
        }
    }

    let corridors = graph
        .iter()
        .filter(|(_, v)| v.len() == 2)
        .map(|(&(y, x), _)| (y, x))
        .collect::<Vec<_>>();

    for (y, x) in corridors.into_iter() {
        let v = graph.remove(&(y, x)).unwrap();

        let ((y1, x1), d1) = v[0];
        let ((y2, x2), d2) = v[1];

        let n1 = graph.get_mut(&(y1, x1)).unwrap();
        if let Some(i) = n1.iter().position(|&((yy, xx), _)| yy == y && xx == x) {
            n1[i] = ((y2, x2), d1 + d2);
        }

        let n2 = graph.get_mut(&(y2, x2)).unwrap();
        if let Some(i) = n2.iter().position(|&((yy, xx), _)| yy == y && xx == x) {
            n2[i] = ((y1, x1), d1 + d2);
        }
    }

    (graph, (y_end as i64 - 1, x_end as i64))
}

fn directions(c: char, slippery: bool) -> Vec<(i64, i64)> {
    if !slippery {
        return vec![(-1, 0), (0, 1), (1, 0), (0, -1)];
    }

    match c {
        '.' => vec![(-1, 0), (0, 1), (1, 0), (0, -1)],
        '^' => vec![(-1, 0)],
        '>' => vec![(0, 1)],
        'v' => vec![(1, 0)],
        '<' => vec![(0, -1)],
        _ => unreachable!(),
    }
}

fn find_longest_path(graph: &HashMap<(i64, i64), Vec<((i64, i64), i64)>>, end: (i64, i64)) -> i64 {
    let mut longest_path = 0;
    let mut queue = VecDeque::from([(HashSet::from([(1, 1)]), (1, 1), 0)]);

    while let Some((path, (y, x), d)) = queue.pop_front() {
        if (y, x) == end {
            longest_path = longest_path.max(d);
            continue;
        }

        for &((new_y, new_x), dd) in graph.get(&(y, x)).unwrap().iter() {
            if !path.contains(&(new_y, new_x)) {
                let mut new_path = path.clone();
                new_path.insert((new_y, new_x));
                queue.push_front((new_path, (new_y, new_x), d + dd));
            }
        }
    }

    longest_path + 2
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 94);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 154);
    }
}
