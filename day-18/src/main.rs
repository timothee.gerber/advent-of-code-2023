fn part_1(input: &str) -> i64 {
    let instructions = parse_dig_plan(input);

    dig(&instructions)
}

fn part_2(input: &str) -> i64 {
    let instructions = parse_dig_plan_correctly(input);

    dig(&instructions)
}

fn parse_dig_plan(input: &str) -> Vec<((i64, i64), i64)> {
    input
        .trim()
        .lines()
        .map(|line| {
            let values: Vec<&str> = line.split(' ').collect();

            let direction = match values[0] {
                "U" => (0, 1),
                "R" => (1, 0),
                "D" => (0, -1),
                "L" => (-1, 0),
                _ => unreachable!(),
            };

            (direction, values[1].parse().unwrap())
        })
        .collect()
}

fn parse_dig_plan_correctly(input: &str) -> Vec<((i64, i64), i64)> {
    input
        .trim()
        .lines()
        .map(|line| {
            let (_, hex) = line.split_once('#').unwrap();

            let length = i64::from_str_radix(&hex[0..5], 16).unwrap();

            let direction = match hex.chars().nth(5).unwrap() {
                '3' => (0, 1),
                '0' => (1, 0),
                '1' => (0, -1),
                '2' => (-1, 0),
                _ => unreachable!(),
            };

            (direction, length)
        })
        .collect()
}

/// Shoelace algorithm
fn dig(instructions: &[((i64, i64), i64)]) -> i64 {
    let mut position = (0, 0);
    let (mut perimeter, mut area) = (0, 0);

    for ((dx, dy), length) in instructions {
        let next_position = (position.0 + dx * length, position.1 + dy * length);

        perimeter += length;
        area += (position.1 + next_position.1) * (position.0 - next_position.0);

        position = next_position;
    }

    (area.abs() + perimeter) / 2 + 1
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 62);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 952408144115);
    }
}
