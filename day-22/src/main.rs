use std::{collections::HashSet, ops::RangeInclusive};

fn part_1(input: &str) -> usize {
    let mut bricks = parse(input);

    bricks.sort_by_key(|b| b.2.start().clone());

    let mut floor = [[(0, 0); 10]; 10];
    let mut disintegratable = vec![true; bricks.len() + 1];
    disintegratable[0] = false; // floor

    for (i, (xx, yy, zz)) in bricks.iter().enumerate() {
        let mut z = 0;

        for x in xx.clone() {
            for y in yy.clone() {
                z = z.max(floor[x as usize][y as usize].0);
            }
        }

        let next_z = z + zz.end() - zz.start() + 1;
        let mut touches = HashSet::new();

        for x in xx.clone() {
            for y in yy.clone() {
                if floor[x as usize][y as usize].0 == z {
                    touches.insert(floor[x as usize][y as usize].1);
                }
                floor[x as usize][y as usize] = (next_z, i + 1);
            }
        }

        if touches.len() == 1 {
            for brick_id in touches.drain() {
                disintegratable[brick_id] = false;
            }
        }
    }

    disintegratable.iter().filter(|b| **b == true).count()
}

fn parse(
    input: &str,
) -> Vec<(
    RangeInclusive<u16>,
    RangeInclusive<u16>,
    RangeInclusive<u16>,
)> {
    input
        .trim()
        .lines()
        .map(|line| {
            let (start, end) = line.split_once('~').unwrap();
            let start = start
                .split(',')
                .map(|n| n.parse::<u16>().unwrap())
                .collect::<Vec<u16>>();
            let end = end
                .split(',')
                .map(|n| n.parse::<u16>().unwrap())
                .collect::<Vec<u16>>();
            (start[0]..=end[0], start[1]..=end[1], start[2]..=end[2])
        })
        .collect()
}

fn part_2(input: &str) -> usize {
    let mut bricks = parse(input);

    bricks.sort_by_key(|b| b.2.start().clone());

    let mut floor = [[(0, 0); 10]; 10];
    let mut up = vec![HashSet::new(); bricks.len() + 1];
    let mut down = vec![HashSet::new(); bricks.len() + 1];
    let mut do_not_cascade = vec![true; bricks.len() + 1];
    do_not_cascade[0] = false; // floor

    for (i, (xx, yy, zz)) in bricks.iter().enumerate() {
        let mut z = 0;

        for x in xx.clone() {
            for y in yy.clone() {
                z = z.max(floor[x as usize][y as usize].0);
            }
        }

        let next_z = z + zz.end() - zz.start() + 1;

        for x in xx.clone() {
            for y in yy.clone() {
                if floor[x as usize][y as usize].0 == z {
                    down[i + 1].insert(floor[x as usize][y as usize].1);
                    up[floor[x as usize][y as usize].1].insert(i + 1);
                }
                floor[x as usize][y as usize] = (next_z, i + 1);
            }
        }

        if down[i + 1].len() == 1 {
            for brick_id in down[i + 1].iter() {
                do_not_cascade[*brick_id] = false;
            }
        }
    }

    let idx = do_not_cascade
        .iter()
        .enumerate()
        .skip(1)
        .filter_map(|(i, condition)| if !condition { Some(i) } else { None })
        .collect::<Vec<usize>>();

    idx.into_iter().map(|idx| count(idx, &up, &down)).sum()
}

fn count(idx: usize, up: &[HashSet<usize>], down: &[HashSet<usize>]) -> usize {
    let mut falling = HashSet::from([idx]);
    let mut next = up[idx].clone();

    while !next.is_empty() {
        let mut new_next = HashSet::new();
        for &brick in next.iter() {
            if falling.is_superset(&down[brick]) {
                falling.insert(brick);
                new_next.extend(&up[brick]);
            }
        }

        next = new_next;
    }

    falling.len() - 1
}

fn main() {
    let input = include_str!("input");

    dbg!(part_1(input));
    dbg!(part_2(input));
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn test_part_1() {
        assert_eq!(part_1(include_str!("example")), 5);
    }

    #[test]
    fn test_part_2() {
        assert_eq!(part_2(include_str!("example")), 7);
    }
}
